'use strict';


// Declare app level module which depends on filters, and services
angular.module('memoryApp', [
        'ngRoute',
        'ngAnimate',
        'memoryApp.services',
        'memoryApp.directives',
        'memoryApp.controllers'
    ])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/intro', {templateUrl: 'partials/intro.html', controller: 'IntroController'});
        $routeProvider.when('/game', {templateUrl: 'partials/game.html', controller: 'GameboardController'});
        $routeProvider.otherwise({redirectTo: '/intro'});
    }])
    .run(function() {
        FastClick.attach(document.body);
    });
