'use strict';

/* Services */

angular.module('memoryApp.services', [])
    /** returns app version */
    .value('version', '0.2')

    /** creates and returns arrays of tile items */
    .service('TileService', function(){
        /** returns array of memory tiles (random subset) from all tiles
         * @param {integer} numItems - size of subset of tiles
         * @param {integer} totalItems - size of array
         * */
        function create(numItems, totalItems){
            var i,                  //counter
                items = [],         //array of all items
                selection = [];     //array of final randomized selection
            //populate items array, then randomize
            for (i=1; i<=totalItems; i+=1){
                items.push(i);
            }
            items.shuffle();
            //take first numItems of items, so no repeats
            for (i=0; i<numItems; i+=1){
                selection.push(items[i]);
            }
            return selection;
        }

        /** returns an array of all tile items
         * @param {integer} numMemoryTiles - size of subset of tiles
         * @param {integer} totalTiles - size of array
         * */
        this.generateTiles = function(numMemoryTiles, totalTiles){
            var i,              //counter
                isMemoryTile = false,   //boolean for if item is part of selection
                memoryTiles = create(numMemoryTiles, totalTiles),   //randomized items for game tiles
                tiles = [];     //array to return that has all the tiles
            console.log('Memory Tiles: ', memoryTiles);
            //generate array
            for (i=1; i<=totalTiles; i+=1){
                //if current item is a memoryTile, mark it as such
                isMemoryTile = (memoryTiles.indexOf(i) >= 0);
                tiles.push({id:i, selected:false, memoryTile:isMemoryTile});
            }
            return tiles;
        };
    });
