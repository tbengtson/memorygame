'use strict';

/* Controllers */

angular.module('memoryApp.controllers', ['memoryApp.services','ngAnimate'])
    .controller('IntroController', ['$scope', '$location', function($scope, $location) {
        /** navigates to new location
         * @param path
         */
        $scope.go = function (path) {
            $location.path(path);
        };
    }])

    /** This controller sets up and manages the tiles of the gameboard */
    .controller('GameboardController', ['$scope', '$timeout', 'TileService', '$animate', function($scope, $timeout, TileService, $animate) {
        //configurable variables
        $scope.totalTiles = 25;
        $scope.memoryTilesPercentage = 0.35;  //% of totalTiles that will be memory tiles
        $scope.tilePreviewTime = 5000;        //time in ms for how long memory tiles are shown before they fade out and game starts
        $scope.gameWonMessage = "You Won!";   //message to show when user found all memory tiles
        $scope.gameLostMessage = "BooHoo";    //message to show when user selected any non-memory tile

        //non-configurable variables
        $scope.numMemoryTiles = Math.round($scope.totalTiles * $scope.memoryTilesPercentage);    //number of playable tiles, rounded

        function init(){
            //state variables
            $scope.numSelected = 0; //number of items
            $scope.tiles = [];      //array of all tile cells
            $scope.modalShown = false;
        }

        /** resets the gameboard to start a new game */
        $scope.reset = function(){
            init();
            //setup the gameboard
            $scope.tiles = TileService.generateTiles($scope.numMemoryTiles, $scope.totalTiles);
            $scope.startGame();   //restart the game
        };

        /** fades out the memory tiles (e.g. to start the game) */
        $scope.hideMemoryTiles = function(){
            $scope.fadeMemoryTiles = 'fade-out';
        };

        /** fades-in the memory tiles (e.g. to reset the game) */
        $scope.showMemoryTiles = function(){
           $scope.fadeMemoryTiles = '';
        };

        /** start the game
         * @param {integer} timeDelay - duration of memoryTile preview in ms
         */
        $scope.startGame = function(timeDelay){
            //a non-default delay can be provided
            if (timeDelay === undefined){ timeDelay = $scope.tilePreviewTime; }
            $scope.showMemoryTiles();
            //hide memory tiles after delay
            $timeout(function() {
                $scope.hideMemoryTiles();
            }, timeDelay);
        };

        /** ends the game and displays a message
         * @param {string} message - text to display in modal
         */
        $scope.endGame = function(message){
            $scope.modalMessage = message;
            $scope.toggleModal();
        };

        /** handle event for when tile is selected
         * @param {object} tile - selected tile
         * @param {object} $event - event object
         * @returns {boolean} - whether game is over or not
         */
        $scope.select = function(tile, $event){
            if (tile.selected === false){
                //check if memoryTile
                if (tile.memoryTile === true){
                    //check if game is won (i.e. last memoryTile was just selected)
                    if ($scope.numMemoryTiles - $scope.numSelected === 1){
                        //if all tiles found, present message
                        $scope.endGame($scope.gameWonMessage);
                    } else {
                        //otherwise, increment counter and continue
                        $scope.numSelected+=1;
                        //mark as selected
                        tile.selected = true;
                        console.log('Found memoryTile '+$scope.numSelected+', but '+($scope.numMemoryTiles-$scope.numSelected)+' more to find');
                    }
                    return true;
                } else {
                    //if not a memory tile, game is lost and present message
                    $scope.endGame($scope.gameLostMessage);
                    return false;
                }
            } else {
                //if tile is already selected, do nothing
                return false;
            }
        };

        /** handles state of the modal */
        $scope.toggleModal = function() {
            $scope.modalShown = !$scope.modalShown;
        };

        //initialize view
        $scope.reset();
    }]);
