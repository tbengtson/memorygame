'use strict';

/* Directives */

angular.module('memoryApp.directives', [])
    /** populates element with version number */
    .directive('appVersion', ['version', function(version) {
        return function(scope, elm, attrs) {
          elm.text(version);
        };
    }])

    /** creates template for modal dialog */
    .directive('modalDialog', function() {
        return {
            restrict: 'E',      // Only matches element name
            replace: true,      // Replace with the template below
            transclude: true,   // Insert custom content inside the directive
            link: function(scope, element, attrs) {
                scope.hideModal = function() {
                    scope.modalShown = false;
                };
            },
            templateUrl: 'partials/modal.html'
        };
    });
