'use strict';

/* jasmine specs for controllers */

describe('controllers', function(){
  beforeEach(module('memoryApp.controllers'));

    it('should IntroController defined', inject(function($controller) {
        //spec body
        var IntroController = $controller('IntroController');
        expect(IntroController).toBeDefined();
    }));
    it('should have GameboardController defined', inject(function($controller) {
        var GameboardController = $controller('GameboardController');
        expect(GameboardController).toBeDefined();
    }));
});
