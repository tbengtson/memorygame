'use strict';

/* jasmine specs for services  */

describe('service', function() {
  beforeEach(module('memoryApp.services'));


  describe('version', function() {
    it('should return current version', inject(function(version) {
      expect(version).toEqual('0.1');
    }));
  });

    describe('TileService', function(){
        xit('should have TileService defined', function(){
            expect().toBeDefined();
        });
        it('should provide a tiles object', function () {
            expect(TileService.generateTiles() instanceof Object).toBe(true);
        });
    });
});
