'use strict';

/* Scenario tests for Memory Game */

describe('memory app', function() {
    browser.get('index.html');

    it('should automatically redirect to /intro when location hash/fragment is empty', function() {
        expect(browser.getLocationAbsUrl()).toMatch("/intro");
    });

    it('should automatically redirect to /intro when location hash/fragment is invalid', function () {
        browser.get('#/foo-bar-bas');
        expect(browser.getLocationAbsUrl()).toMatch('/intro');
    });

    it('should not automatically redirect to /intro when location hash/fragment is valid', function () {
        browser.get('#/intro');
        expect(browser.getLocationAbsUrl()).toMatch('/intro');
    });

    describe('introView', function() {
        beforeEach(function() {
            browser.get('index.html#/intro');
        });

        it('should render intro when user navigates to /intro', function() {
            expect(element.all(by.css('[ng-view] h2')).first().getText())
                .toMatch(/How to Play/);
        });

        it('should have a button to start game', function() {
            expect(element.all(by.css('[ng-view] a')).first().getText())
                .toMatch(/Start Game/);
        });

    });


    describe('gameView', function() {

        beforeEach(function() {
            browser.get('index.html#/game');
        });


        it('should render game when user navigates to /game', function() {
            expect(element.all(by.css('[ng-view] li div')).first().getText())
                .toMatch(/1/);
        });

        it('should render 25 squares', function() {
            expect(element.all(by.css('[ng-view] li')).count())
                .toBe(25);
        });

        it('should render 9 selected squares', function() {
            expect(element.all(by.css('[ng-view] .memory')).count())
                .toBe(9);
        });

        it('should render selected squares visually different', function() {
            expect(element.all(by.css('[ng-view] .memory')).first().getCssValue('background-color'))
                .toMatch(/rgba\(218, 55, 58,.*/);
        });

        it('should hide selected squares after 5 seconds', function() {
            expect(element.all(by.css('[ng-view] .memory')).first().getCssValue('background-color'))
                .toMatch(/rgba\(218, 55, 58,.*/);
            setTimeout(function(){
                expect(element.all(by.css('[ng-view] .memory')).first().getCssValue('background-color'))
                    .toMatch(/rgba\(255, 252, 196,.*/);
            },5500);
        });

        it('should render modal when game is won', function() {
            expect(element.all(by.css('[ng-view] .modalClass')).count())
                .toBe(1);
        });

        it('should render modal when game is lost', function() {
            expect(element.all(by.css('[ng-view] .modalClass')).count())
                .toBe(1);
        });

        it('should reset game when modal is closed', function() {
            //trigger modal
            $('[ng-view] li div:not(.memory)').click();
            //element.all(by.css('[ng-view] li div:not(.memory)').click());

            //click restart button
            $('[ng-view] .ng-modal button').click();
            //element.all(by.css('[ng-view] ng-modal button').click());
            expect(element.all(by.css('[ng-view] .memory')).first().getCssValue('background-color'))
                .toMatch(/rgba\(218, 55, 58,.*/);
        });

  });
});
