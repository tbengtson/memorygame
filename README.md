# Memory Game

This project is a simple memory game built using AngularJS.

It is designed for mobile, but responsive enough to work on most devices.


## Dependencies

The app should be built with (e.g. bower install):
    angular
    angular-route
    angular-animate
    fastclick



## Testing

Some rudimentary Jasmine unit tests and Protractor e2e tests are also included.
